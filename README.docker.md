# Run via docker

First, build project, see README.md.

Then run docker-compose :

    docker-compose up --force-recreate


You can then access thermomix-recipes on http://localhost:8000